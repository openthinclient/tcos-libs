error-window-title = Fehler
error-title = Fehler
info-window-title = Information
info-title = Information
configuration-error-title = Konfigurationsfehler
mount-home-failed = Konnte persistentes Home nicht mounten.
mount-home-no-partition = Persistentes Home von lokalem Speicher wurde konfiguriert, aber es wurde keine Partition angegeben.
mount-home-fallback-to-RAM = Home im RAM is aktiv. Benutzerdaten werden nicht gespeichert.
touchscreen-calibration-title = Touchscreen-Kalibrierung
touchscreen-calibration-window-title = Touchscreen-Kalibrierung
touchscreen-calibration-swap-axes-yes = Ja
touchscreen-calibration-swap-axes-no = Nein
touchscreen-calibration-message = Bitte,
    1) starten Sie das openthinclient-Management

    2) legen Sie dort eine neue Komponente vom Type Touchscreen-Kalibrierung an

    3) kopieren Sie folgende Werte in die Konfiguration:

        Produktname: { $product }
        Kalibrierungsdaten: { $calibration }
        Achsen vertauschen: { $axes }

    4) weisen Sie die neue Komponente dem Thinclient zu
sso-check-error-message = Single Sign-On und Autologin sind aktiviert. Diese beiden Einstellungen sind nicht kompatibel. Single Sign-On wird nicht funktionieren.
    Bitte informieren Sie Ihren Administrator
audio-sink-not-found = Audioausgabegerät "{ $sink }" wurde nicht gefunden.


    Verfügbare Ausgabegeräte:

    { $sinks }
audio-source-not-found = Audioeingabegerät "{ $source }" wurde nicht gefunden.


    Verfügbare Audioeingabegeräte:

    { $sources }
audio-profile-not-found = Audio-Profil "{ $profile }" für Soundkarte #{ $card_n } ({ $card }) wurde nicht gefunden.


    Verfügbare Audio-Profile:

    { $profiles }
audio-bell-sound-not-found = Sounddatei des System Bell "{ $path }" wurde nicht gefunden.
display-connector-not-found = Display-Anschluss "{ $connector }" wurde nicht gefunden.


    Verfügbare Display-Anschlüsse:

    { $connectors }
usage-authorization-warning-title = Problem mit Nutzungsberechtigung
usage-authorization-too-old = Es liegt eine Kommunikationsstörung dieser openthinclient Installation mit dem Autorisierungsserver vor oder die Nutzungsberechtigung ist abgelaufen.


    Bitte informieren Sie Ihren Administrator.
usage-authorization-expired = Es liegt ein kritischer Fehler vor.

    Die Laufzeit der Nutzungsberechtigung dieser openthinclient Installation ist abgelaufen.


    Bitte informieren Sie Ihren Administrator.
usage-authorization-too-many = Es werden mehr Thin Clients verwendet als die Nutzungsberechtigung zulässt.


    Bitte informieren Sie Ihren Administrator.
usage-authorization-invalid = Es liegt ein kritischer Fehler vor.

    Die Nutzungsberechtigung ist nicht kompatibel mit der Server ID dieser openthinclient Installation.


    Bitte informieren Sie Ihren Administrator.
usage-authorization-missing = Diese openthinclient Installation wird mit 25 oder mehr ThinClients verwendet.


    Bitte informieren Sie Ihren Administrator, diese Installation zu registrieren und eine Nutzungsberechtigung zu erwerben.

restart-required-title = Neustart notwendig
restart-required-message = Das Betriebssystem oder Applikationen wurden aktualisiert.

    Bitte starten Sie diesen ThinClient baldmöglichst neu.

ip-changed-title = DHCP Problem erkannt!
ip-changed-message = Diesem ThinClient wurde im laufenden Betrieb eine neue IP Adresse zugewiesen. Dies kann zur Instabilität des Systems führen.

    Bitte informieren Sie Ihren Administrator.
