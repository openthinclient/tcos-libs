error-window-title = Error
error-title = Error
info-window-title = Information
info-title = Information
configuration-error-title = Configuration error
mount-home-failed = Could not mount persistent home.
mount-home-no-partition = Persistent home from local storage was configured, but no partition was specified.
mount-home-fallback-to-RAM = Home in RAM is active. User data will not be saved.
touchscreen-calibration-title = Touchscreen calibration
touchscreen-calibration-window-title = Touchscreen calibration
touchscreen-calibration-swap-axes-yes = Yes
touchscreen-calibration-swap-axes-no = No
touchscreen-calibration-message = Please,
    1) start the openthinclient-Management

    2) create a new Device of type 'Touchscreen-Calibration' there

    3) copy the following values in the configuration:

        Model description: { $product }
        Calibration data: { $calibration }
        Swap axes: { $axes }

    4) assign the new touchscreen component to the thinclient
sso-check-error-message = Single sign-on and autologin are activated. These two settings are not compatible. Single sign-on will not work.
    Please contact your administrator.
audio-sink-not-found = Audio output device "{ $sink }" was not found.


    Available audio output devices:

    { $sinks }
audio-source-not-found = Audio input device "{ $source }" was not found.


    Available audio input devices:

    { $sources }
audio-profile-not-found = Failed to find profile "{ $profile }" for SoundCard #{ $card_n } ({ $card }).


    Available profiles:

    { $profiles }
audio-bell-sound-not-found = Failed to find sound file "{ $path }" for the bell sound.
display-connector-not-found = Failed to find display connector "{ $connector }"


    Available display connectors:

    { $connectors }
usage-authorization-warning-title = Problem with usage authorization
usage-authorization-too-old = There is a communication problem of this openthinclient installation with the authorization server or the usage authorization has expired.

    Please inform your administrator.
usage-authorization-expired = There is a critical error.

    The usage authorization of this openthinclient installation has expired.

    Please inform your administrator.
usage-authorization-too-many = More thin clients are used than the usage authorization allows.

    Please inform your administrator.
usage-authorization-invalid = There is a critical error.

    The usage authorization is not compatible with the server ID of this openthinclient installation.

    Please inform your administrator.
usage-authorization-missing = This openthinclient installation is being used with 25 thin clients or more.

    Please inform your administrator to register the installation and purchase a usage authorization.

restart-required-title = Restart required
restart-required-message = The operating system or applications have been updated.

    Please restart this thin client as soon as possible.

ip-changed-title = DHCP problem detected!
ip-changed-message = This thin client has been assigned a new IP address during operation. This can lead to instability of the system.

    Please inform your administrator.
