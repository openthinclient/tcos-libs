from collections.abc import Iterable
import os
from pathlib import Path
from types import MappingProxyType
from typing import Optional, Union


boot_arguments = MappingProxyType(dict(
    param.split('=', 1) if '=' in param else (param, True)
    for param in Path('/proc/cmdline').read_text().split()
))
is_localboot = boot_arguments.get('localboot') == 'true'
is_installer = 'localboot_installer' in boot_arguments
server       = boot_arguments.get('management_server')

log_level = os.getenv('LOGGING', boot_arguments.get('LOGGING', '')).upper()
if log_level not in ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'):
    log_level = 'WARNING'


def _read_etc_env():
    """ Read /etc/environment and return a dictionary of key-value pairs.

        This is used used as a fallback if not all required environment
        variables are set in the current process.
    """
    env = {}
    with open('/etc/environment') as f:
        for line in f:
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            key, val = line.split('=', 1)
            env[key] = val
    return env

def tcos_mac_from_nic():
    """ MAC address (for client ID) of boot NIC. Defaulting to first NIC. """
    env = os.environ if 'TCOS_NIC' in os.environ else _read_etc_env()
    tcos_mac = env.get('TCOS_MAC')
    if tcos_mac:
        return ':'.join(tcos_mac[i:i+2] for i in range(0, 12, 2))
    tcos_nic = env.get('TCOS_NIC', 'eth0')
    if (addr := Path('/sys/class/net', tcos_nic, 'address')).exists():
        return addr.read_text().strip()
    elif (addr := Path('/sys/class/net/wlan0/address')).exists():
        return addr.read_text().strip()

def get_tcos_mac():
    """ TCOS_MAC for client ID. """
    if (tcos_mac := tcos_mac_from_nic()):
        return tcos_mac
    elif (client_id := Path('/tcos/link/localboot/client_id')).exists():
        return client_id.read_text().strip()
    else:
        raise ValueError('Could not determine TCOS MAC address')

tcos_mac = get_tcos_mac()
