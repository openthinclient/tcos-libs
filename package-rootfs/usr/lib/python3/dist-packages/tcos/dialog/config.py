from types import SimpleNamespace
import os
import yaml

BASE_PATH = os.path.dirname(os.path.realpath(__file__))

SOCKET_PATH='/run/user/1000/tcos-dialog.sock'

RETURN_CODES  = SimpleNamespace(
    OK           = 0,
    CANCEL       = 1,
    DESTROY      = 2,
    TIMEOUT      = 3,
    ERROR        = 64,
    CONFIG_ERROR = 65,
)

ICON_NAMES = {
    'error':    'dialog-error',
    'info':     'dialog-information',
    'question': 'dialog-question',
    'warning':  'dialog-warning',
}

DIALOG_TYPES = [
    'balloon',
    'confirmation',
    'login',
    'monitors',
]

DEFAULTS = {
    'dir':              None,
    'type':             'confirmation',
    'window_title_key': 'window-title',
    'title_key':        None,
    'message_key':      None,
    'error_key':        None,
    'confirm_key':      'button-confirm',
    'cancel_key':       'button-cancel',
    'icon':             'none',
    'no_cancel':        False,
    'no_confirm':       False,
    'keep_above':       False,
    'fields':           [],
    'required':         [],
    'values':           {},
    'close_timeout':    None,
    'defaults':         {}
}

class ConfigurationError(Exception):
    pass


def build_cfg(args):
    if not args.get('dir'):
        raise ConfigurationError('No directory specified')
    dialog_type = args.get('type')
    if dialog_type is None:
        raise ConfigurationError('No dialog type specified')

    if dialog_type in DIALOG_TYPES:
        cfg_from_file = {}
    else:
        cfg_path = os.path.join(args['dir'], 'config.yml')

        if not os.path.exists(cfg_path):
            raise ConfigurationError(
                    f'Unknown dialog type {dialog_type}. {cfg_path} not found.')

        try:
            with open(cfg_path, 'r') as cfg_path:
                config = yaml.safe_load(cfg_path)
        except Exception as ex:
            raise ConfigurationError(f'Could not load {cfg_path}: {ex}')

        cfg_from_file = config.get(dialog_type, {}) if config else {}
        del args['type']    # don't overwrite type from config below

        if not cfg_from_file:
            raise ConfigurationError(
                    f'Dialog type {dialog_type} not found in {cfg_path.name}')

    cfg_from_args = { key: args.get(key)
                      for key in DEFAULTS
                      if args.get(key) not in (None, False)}

    cfg = { **DEFAULTS, **cfg_from_file, **cfg_from_args }

    if cfg.get('type') not in DIALOG_TYPES:
        raise ConfigurationError(f'Unknown dialog type {cfg.get("type")}')

    return cfg
