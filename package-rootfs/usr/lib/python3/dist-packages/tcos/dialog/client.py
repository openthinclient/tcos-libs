import os
import socket
import time

import yaml

from tcos import logger

from .config import RETURN_CODES, SOCKET_PATH

MAX_RETRIES=30 # deciseconds


def read_dialog_socket(dialog_config):
    for attempt in range(MAX_RETRIES):
        try:
            data = _read_dialog_socket(dialog_config)
        except OSError as ex:
            if attempt == MAX_RETRIES - 1:
                raise ex
            if attempt % 10 == 0:
                logger.get().warning(
                        f'Failed to connect to dialog socket: {ex.strerror}\n'
                        f'Retrying... (Attempt {attempt+1}/{MAX_RETRIES})')
            time.sleep(.1)
            continue
        try:
            return yaml.safe_load(data)
        except Exception as ex:
            logger.get().exception(
                    f'Failed to parse dialog response: {data}', ex)
            raise ex


def _read_dialog_socket(dialog_config):
    chunks = []
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        sock.connect(SOCKET_PATH)
        sock.sendall(yaml.dump(dialog_config).encode('utf-8'))
        sock.send(b'---\n')

        while True:
            chunk = sock.recv(4096)
            if not chunk:
                return b''.join(chunks)
            chunks.append(chunk)


def open_dialog(**dialog_config):
    for key in ('dir', 'type'):
        if key not in dialog_config:
            raise ValueError(f'{key} argument is required')

    if not dialog_config.get('detach') and 'pid' not in dialog_config:
        dialog_config['pid'] = os.getpid()

    return read_dialog_socket(dialog_config)


def destroy_dialog(dialog_id):
    return read_dialog_socket({'destroy': dialog_id})


def update_dialog(dialog_id, **config):
    config['update'] = dialog_id
    return read_dialog_socket(config)


class DetachedDialog:
    _closed = False

    def __new__(cls, dir, type, message_key=None, **kwargs):
        dialog = super().__new__(cls)
        result = open_dialog(
            dir=dir,
            type=type,
            message_key=message_key,
            detach=True,
            **kwargs
        )
        if result['code'] != RETURN_CODES.OK:
            logger.error('Failed to open dialog: %s', result['result'])
            dialog._closed = True
        else:
            dialog.dialog_id = result['result']
        return dialog

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @property
    def closed(self):
        if not self._closed:
            result = update_dialog(self.dialog_id)
            self._closed = result['code'] != RETURN_CODES.OK
        return self._closed

    def close(self):
        destroy_dialog(self.dialog_id)
        self._closed = True

    def update(self, message_key, **values):
        result = update_dialog(
                self.dialog_id, message_key=message_key, values=values)
        if result['code'] != RETURN_CODES.OK:
            self._closed = True
            return False
        return True
