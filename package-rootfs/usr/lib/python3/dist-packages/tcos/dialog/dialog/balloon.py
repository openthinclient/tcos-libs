from ..config import ICON_NAMES
from .dialog import Dialog

class BalloonDialog(Dialog):
    I18N_DOMAIN = 'balloon'
    UI_FILE_PATH = 'ui/balloon.ui'

    def __init__(self, conf, callback):
        super().__init__(conf, callback)

        self.title = self.builder.get_object('title')
        self.text  = self.builder.get_object('message')
        self.icon  = self.builder.get_object('icon')

        self.update(init=True)

        self.window.set_keep_above(True)

        self.window.connect('button-press-event', lambda w, e: self.exit())
        self.window.connect('size-allocate', lambda w, a: self._moveNE())


    def update(self, config={}, init=False):
        values = config.get('values', {})
        if init:
            self.config.update(config)
            config = self.config

        if 'title_key' in config or init:
            if title_key := config['title_key']:
                self.title.set_text(self.t(title_key, values))
            self.title.set_visible(bool(title_key))

        if 'message_key' in config or init:
            if message_key := config['message_key']:
                self.text.set_text(self.t(message_key, values))
            self.text.set_visible(bool(message_key))

        if 'icon' in config or init:
            if icon_name := ICON_NAMES.get(self.config['icon']):
                self.icon.set_from_icon_name(icon_name, 60)
                self.window.set_icon_name(icon_name)
            self.icon.set_visible(bool(icon_name))


    def _moveNE(self):
        screen = self.window.get_screen()
        active_window = screen.get_active_window()
        if active_window is None:
            monitor = screen.get_primary_monitor()
        else:
            monitor = screen.get_monitor_at_window(active_window)
        monitor_geometry = screen.get_monitor_geometry(monitor)
        width, _ = self.window.get_size()
        self.window.move(
            monitor_geometry.width - width - 5,
            monitor_geometry.y + 42
        )
