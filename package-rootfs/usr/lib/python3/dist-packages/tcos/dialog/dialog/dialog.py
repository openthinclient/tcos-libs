import os
import re
import time

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import GLib, Gdk, Gtk

from abc import ABC, abstractmethod

from ..translation import Translation
from ..config import BASE_PATH, RETURN_CODES

ENTER_KEYS = (
    Gdk.KEY_Return,
    Gdk.KEY_KP_Enter,
    Gdk.KEY_ISO_Enter,
    Gdk.KEY_3270_Enter,
)

SPLIT = re.compile(r'[,\s]+').split

class Dialog(ABC):
    @property
    @abstractmethod
    def UI_FILE_PATH(self):
        raise NotImplementedError()

    @property
    @abstractmethod
    def I18N_DOMAIN(self):
        raise NotImplementedError()

    def __init__(self, config, callback=None):
        self.exited = False

        self.callback = callback
        self.config = config
        self.translate = Translation(
                config['dir'], self.I18N_DOMAIN, config['values']
            ).translate
        ui_file = os.path.join(BASE_PATH, self.UI_FILE_PATH)

        self.builder = Gtk.Builder()
        self.builder.add_from_file(ui_file)

        self.window = self.builder.get_object('window')


    def update(self, config={}, init=False):
        pass


    def t(self, keys, values={}):
        """ Translate from i18n key or comma-separated list of keys. """
        return '\n\n'.join(self.translate(key, values) for key in SPLIT(keys))


    def show(self):
        if self.config.get('keep_above'):
            self.window.set_keep_above(True)
            self.window.set_skip_taskbar_hint(True)
            self.window.set_skip_pager_hint(True)

            # Disable window controls
            self.window.set_modal(True)
            self.window.set_deletable(False)
            self.window.set_resizable(False)

            # Disable quick close with ESC
            self.window.connect('key-press-event',
                                lambda w, e: e.keyval == Gdk.KEY_Escape)

            def stay_on_top():
                if self.exited:
                    return False
                if not self.window.is_active():
                    self.window.present_with_time(time.monotonic() * 1000)
                return True

            GLib.timeout_add(300, stay_on_top)

        self.window.present_with_time(time.monotonic() * 1000)


    def destroy(self):
        self.exited = True
        self.window.close()


    def exit(self, code=RETURN_CODES.OK, result=None):
        if self.exited:
            return
        self.exited = True

        self.window.close()

        if self.callback:
            self.callback(self, code, result)
