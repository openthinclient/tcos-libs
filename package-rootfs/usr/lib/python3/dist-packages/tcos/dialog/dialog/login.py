from ..config import RETURN_CODES
from .dialog import Dialog, ENTER_KEYS

class LoginDialog(Dialog):
    I18N_DOMAIN   = 'login'
    UI_FILE_PATH  = 'ui/login.ui'

    def __init__(self, cfg, callback):
        super().__init__(cfg, callback)

        self.required = cfg['required']
        self.fields = cfg['fields']

        self.window.set_title(self.t('window-title'))

        label_title = self.builder.get_object('label_title')
        if title_key := cfg['title_key']:
            label_title.set_text(self.t(title_key))
        label_title.set_visible(bool(title_key))

        box_main = self.builder.get_object('box_main')
        self.entries = {}
        for i, field in enumerate(self.fields):
            entry = self._init_field(field, cfg['values'])
            self.entries[field] = entry

            entry_container = entry.get_parent()
            box_main.reorder_child(entry_container, i)
            entry_container.show()

        label_error = self.builder.get_object('label_error')
        if cfg['error_key'] is not None:
            if error_key := cfg['error_key']:
                label_error.set_text(self.t(error_key))
            label_error.set_visible(bool(error_key))

        self.button_confirm = self.builder.get_object('button_confirm')
        self.button_confirm.set_label(self.t(self.config['confirm_key']))
        self.button_confirm.connect('clicked', lambda *_: self._confirm())

        self.button_cancel = self.builder.get_object('button_cancel')
        self.button_cancel.set_label(self.t(self.config['cancel_key']))
        self.button_cancel.connect(
                'clicked', lambda *_: self.exit(RETURN_CODES.CANCEL))

        self._focus_first_empty_entry()

        self.window.connect(
                'destroy', lambda *_: self.exit(RETURN_CODES.DESTROY))


    def _init_field(self, field, defaults):
        is_required = field in self.required

        label_text = self.t(f'label-{field}')
        if is_required:
            label_text += '*'
        self.builder.get_object(f'label_{field}').set_text(label_text)

        entry = self.builder.get_object(f'entry_{field}')
        entry.set_text(defaults.get(field, ''))
        entry.connect('key-press-event', self._on_enter)
        if is_required:
            entry.connect('changed', self._clear_error_mark)

        return entry


    def _clear_error_mark(self, entry):
        entry.get_style_context().remove_class('invalid')


    def _confirm(self):
        required_fields_left = False
        for field in self.required:
            if self.entries[field].get_text().strip() == '':
                self.entries[field].get_style_context().add_class('invalid')
                required_fields_left = True

        if not required_fields_left:
            result = {}
            for field in self.entries:
                result[field] = self.entries[field].get_text()

            self.exit(result=result)


    def _on_enter(self, entry, event):
        if event.keyval not in ENTER_KEYS or self.button_cancel.has_focus():
            return

        entries = [*self.entries.values()]
        index = entries.index(entry)
        # Try to select the next emty entry
        for entry in entries[index + 1:]:
            if entry.get_text().strip() == '':
                entry.grab_focus()
                return

        # Try to select the first required entry that is empty
        for i, field in enumerate(self.fields[:index]):
            if field not in self.required:
                continue
            if self.entries[field].get_text().strip() == '':
                self.entries[field].grab_focus()
                return

        self._confirm()


    def _focus_first_empty_entry(self):
        for entry in self.entries.values():
            if entry.get_text().strip() == '':
                entry.grab_focus()
                return

        self.button_confirm.grab_focus()
