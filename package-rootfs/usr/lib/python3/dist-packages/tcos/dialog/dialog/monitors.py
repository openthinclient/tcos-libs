from dataclasses import dataclass

from .dialog import Dialog

import re
import subprocess


match_monitor_line = re.compile(
    r'(\S+)\s+(?:(?:dis)?connected).+?(\d+)x(\d+)\+(\d+)\+(\d+)\b'
).match


class MonitorsMainDialog(Dialog):
    I18N_DOMAIN = 'monitors'
    UI_FILE_PATH = 'ui/monitors_main.ui'

    def __init__(self, conf, callback):
        super().__init__(conf, callback)

        self.window.set_title(self.t('title-main'))
        self.window.connect('delete-event', lambda *_: self._close())
        self.window.set_modal(True)
        self.window.set_resizable(False)
        self.window.grab_focus()
        self.window.set_keep_above(True)

        info_label = self.builder.get_object('label_main_info')
        info_label.set_text(self.t('main-info'))

        self.monitors = self._get_monitors()
        self.children = []

        conf['values'] = values = {}
        for i, monitor in enumerate(self.monitors, 1):
            values['num'] = i
            values['name'] = monitor.connector
            values['resolution'] = f'{monitor.width}x{monitor.height}'

            mon_dialog = MonitorsInfoDialog(
                conf,
                monitor
            )
            mon_dialog.window.show()
            self.children.append(mon_dialog)

        close = self.builder.get_object('button_close')
        close.set_label(self.t('button-close'))
        close.connect('clicked', lambda *_: self.exit())


    def _close(self):
        for child in self.children:
            child.window.close()

        self.exit()


    def _get_monitors(self):
        command = 'LANG=C xrandr -d :0 | grep " connected "'
        result = subprocess.check_output(command, shell=True) \
                .decode() \
                .strip() \
                .split('\n')

        monitors = []

        for line in result:
            match = match_monitor_line(line)

            if match is None:
                continue

            is_primary = 'primary' in line

            groups = match.groups()
            monitors.append(
                Monitor(
                    groups[0],
                    int(groups[1]),
                    int(groups[2]),
                    int(groups[3]),
                    int(groups[4]),
                    is_primary
                )
            )

        monitors.sort(key=lambda m: m.primary, reverse=True)

        return monitors


class MonitorsInfoDialog(Dialog):
    I18N_DOMAIN = 'monitors'
    UI_FILE_PATH = 'ui/monitors_info.ui'

    def __init__(self, conf, monitor):
        super().__init__(conf)

        self.window.move(monitor.pos_x + 10, monitor.pos_y + 10)
        self.window.set_modal(False)
        self.window.set_decorated(False)
        self.window.set_resizable(False)
        self.window.set_skip_taskbar_hint(True)
        self.window.set_skip_pager_hint(True)
        self.window.set_keep_above(True)

        self.window.set_title(self.t('title-info'))

        monitor_number = self.builder.get_object('monitor_number')
        monitor_number.set_text(self.t('monitor-number'))

        connector_headline = self.builder \
                .get_object('monitor_connector_headline')
        connector_headline.set_text(monitor.connector)

        connector = self.builder.get_object('monitor_connector')
        connector.set_text(self.t('monitor-connector'))

        resolution = self.builder.get_object('monitor_resolution')
        resolution.set_text(self.t('monitor-resolution'))


@dataclass
class Monitor:
    connector: str
    width: int
    height: int
    pos_x: int
    pos_y: int
    primary: bool
