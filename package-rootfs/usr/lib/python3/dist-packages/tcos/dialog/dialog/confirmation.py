from ..config import ICON_NAMES, RETURN_CODES
from .dialog import Dialog, ENTER_KEYS

# The maximum height of the message box in pixels before scroll bars are shown
MAX_MESSAGE_HEIGHT = 320


class ConfirmationDialog(Dialog):
    I18N_DOMAIN   = 'confirmation'
    UI_FILE_PATH  = 'ui/confirmation.ui'

    def __init__(self, config, callback):
        super().__init__(config, callback)

        self.title          = self.builder.get_object('label_title')
        self.box_main       = self.builder.get_object('box_main')
        self.scroll         = self.builder.get_object('scroll')
        self.message        = self.builder.get_object('message')
        self.text_buffer    = self.builder.get_object('text_buffer')
        self.icon           = self.builder.get_object('icon')
        self.button_confirm = self.builder.get_object('button_confirm')
        self.button_cancel  = self.builder.get_object('button_cancel')

        self.update(init=True)

        self.message.connect('size-allocate', self.adjust_message_height)

        self.button_confirm.connect('clicked',
                                    lambda *_: self.exit(RETURN_CODES.OK))
        self.button_cancel.connect('clicked',
                                   lambda *_: self.exit(RETURN_CODES.CANCEL))

        self.window.connect('key-press-event', self.confirm_on_enter)
        self.window.connect(
            'destroy', lambda *_: self.exit(RETURN_CODES.DESTROY))


    def update(self, config={}, init=False):
        values = config.get('values', {})
        if init:
            self.config.update(config)
            config = self.config

        if 'window_title_key' in config:
            self.window.set_title(
                    self.t(config['window_title_key'], values) )

        if 'title_key' in config:
            if title_key := config['title_key']:
                self.title.set_text( self.t(title_key, values) )
            self.title.set_visible(bool(title_key))

        if 'message_key' in config:
            if message_key := config['message_key']:
                self.text_buffer.set_text( self.t(message_key, values) )
            self.box_main.set_visible(bool(message_key))

        if 'icon' in config:
            if icon_name := ICON_NAMES.get(config['icon']):
                self.icon.set_visible(True)
                self.icon.set_from_icon_name(icon_name, 60)
                self.window.set_icon_name(icon_name)
            self.icon.set_visible(bool(icon_name))

        if 'confirm_key' in config:
            self.button_confirm.set_label(
                    self.t(config['confirm_key'], values) )

        if 'no_confirm' in config:
            self.button_confirm.set_visible(not config['no_confirm'])

        if 'cancel_key' in config:
            self.button_cancel.set_label(
                    self.t(config['cancel_key'], values) )

        if 'no_cancel' in config:
            self.button_cancel.set_visible(not config['no_cancel'])

        # Minimize window height
        self.window.resize(self.window.get_size().width, 1)


    @staticmethod
    def _additional_height(widget):
        margin = widget.get_style_context().get_margin(0)
        padding = widget.get_style_context().get_padding(0)
        border = widget.get_style_context().get_border(0)
        return margin.top + margin.bottom + padding.top + padding.bottom + border.top + border.bottom


    def adjust_message_height(self, message_widget, allocation):
        natural_height = message_widget.get_preferred_height().natural_height

        height = min(MAX_MESSAGE_HEIGHT, natural_height)
        if height != allocation.height:
            self.scroll.set_size_request(-1, height + self._additional_height(self.scroll))


    def confirm_on_enter(self, _, event):
        if event.keyval in ENTER_KEYS:
            if not self.button_cancel.has_focus():
                self.button_confirm.clicked()
