from fluent.runtime import FluentBundle, FluentLocalization, FluentResource
import os
from .config import BASE_PATH

BASE_LOCALE_DIR = os.path.join(BASE_PATH, 'base_locales')

lang = os.environ.get('LANG', 'en').split('_')[0]
locales = [lang, 'en'] if lang != 'en' else ['en']

class Translation():
    def __init__(self, locale_dir, i18n_domain, values={}):
        self.values = values
        self.bundles = []

        for locale in locales:
            bundle = FluentBundle([locale])

            base_file = f'{BASE_LOCALE_DIR}/{locale}/{i18n_domain}.ftl'
            if os.path.exists(base_file):
                with open(base_file, 'r') as f:
                    resource = FluentResource('\n'.join(f.readlines()))
                bundle.add_resource(resource)

            app_file = f'{locale_dir}/{locale}.ftl'
            if os.path.exists(app_file):
                with open(app_file, 'r') as f:
                    resource = FluentResource(''.join(f.readlines()))
                bundle.add_resource(resource, allow_overrides=True)

            self.bundles.append(bundle)


    def translate(self, key, values={}):
        for bundle in self.bundles:
            if not bundle.has_message(key):
                continue

            msg = bundle.get_message(key)
            if not msg.value:
                continue

            val, _ = bundle.format_pattern(msg.value, {**self.values, **values})
            return val
        return key
