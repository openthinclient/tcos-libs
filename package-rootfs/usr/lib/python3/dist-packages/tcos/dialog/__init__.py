from .client import (
    open_dialog    as open,
    destroy_dialog as destroy,
    update_dialog  as update,
    DetachedDialog,
)
from .config import RETURN_CODES
return_codes_dict = vars(RETURN_CODES)
globals().update(return_codes_dict)

__all__ = [
    'open',
    'update',
    'destroy',
    'DetachedDialog',
    'RETURN_CODES',
    *return_codes_dict.keys(),
]
