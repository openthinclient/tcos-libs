#!/usr/bin/python3

import fcntl
import yaml
import os
from tcos import logger
from .client import open_dialog
from .config import SOCKET_PATH

DEFFERED_DIALOGS_PATH = '/run/tcos/deferred-dialogs.yml'

def defer(**dialog_config):
    is_new = not os.path.exists(DEFFERED_DIALOGS_PATH)
    os.makedirs(os.path.dirname(DEFFERED_DIALOGS_PATH), exist_ok=True)
    with open(DEFFERED_DIALOGS_PATH, 'a') as f:
        fcntl.flock(f, fcntl.LOCK_EX)
        f.write('---\n')
        f.write(yaml.dump(dialog_config))
        f.flush()
        if is_new:
            try:
                os.chmod(DEFFERED_DIALOGS_PATH, 0o666)
            except:
                pass
        fcntl.flock(f, fcntl.LOCK_UN)


def open_or_defer(**dialog_config):
    if os.path.exists(SOCKET_PATH):
        try:
            return open_dialog(**dialog_config)
        except:
            pass

    return defer(**dialog_config)


def open_deferred_dialogs():
    LOG = logger.get('tcos-dialog-deferred')
    LOG.debug('Opening deferred dialogs')

    if not os.path.exists(DEFFERED_DIALOGS_PATH):
        LOG.debug('No deferred dialogs found')
        return

    with open(DEFFERED_DIALOGS_PATH) as f:
        fcntl.flock(f, fcntl.LOCK_EX)
        raw_yaml = f.read()
        fcntl.flock(f, fcntl.LOCK_UN)

    deferred_dialogs = yaml.load_all(raw_yaml, Loader=yaml.BaseLoader)


    for dialog_config in deferred_dialogs:
        LOG.debug('Opening deferred dialog: %s', dialog_config.get('type'))
        dialog_config['detach'] = True
        open_dialog(**dialog_config)


    try:
        LOG.debug('Removing deferred dialogs file')
        os.remove(DEFFERED_DIALOGS_PATH)
    except:
        LOG.debug('Failed to remove deferred dialogs file.'
                  ' Overwriting it instead')
        open(DEFFERED_DIALOGS_PATH, 'w').close()


if __name__ == '__main__':
    open_deferred_dialogs()
