#!/usr/bin/python3

import argparse
import sys

from .client import open_dialog

class ListAppendAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        items = getattr(namespace, self.dest, None)
        if items is None:
            items = []
        elif type(items) is not list:
            items = [items]
        setattr(namespace, self.dest, [*items, *values.split(',')])


parser = argparse.ArgumentParser(prog='tcos-dialog')
parser.add_argument('dir', help=
    'Directory with en.ftl, de.ftl and possibly a config.yml.')
parser.add_argument('type', help=
    'Type of dialog to show. Either defined in config.yml or one of '
    'the built-in types: confirmation, balloon, login, monitors')
parser.add_argument('--title_key')
parser.add_argument('--window_title_key')
parser.add_argument('--message_key')
parser.add_argument('--error_key')
parser.add_argument('--confirm_key')
parser.add_argument('--cancel_key')
parser.add_argument('--icon',
                    choices=['question', 'info', 'warning', 'error'])
parser.add_argument('--no_cancel', action='store_true')
parser.add_argument('--no_confirm', action='store_true')
parser.add_argument('--keep_above', action='store_true')
parser.add_argument('--detach', action='store_true')
parser.add_argument('--close_timeout', type=int)
parser.add_argument('--fields', action=ListAppendAction)
parser.add_argument('--required', action=ListAppendAction)
parser.add_argument('--pid')


args = vars(parser.parse_args())

values = args.setdefault('values', {})
for line in sys.stdin:
    line = line.strip()

    if len(line) == 0:
        break

    key, value = line.split('=', 1)
    args['values'][key] = value

result = open_dialog(**args)

if not result:
    sys.stderr.write('Failed to open dialog\n')
    sys.exit(64)

if result['result']:
    if result['code'] >= 64:
        sys.stderr.write(f'{result["result"]}\n')
    elif isinstance(result['result'], dict):
        for key, value in result['result'].items():
            sys.stdout.write(f'{key}={value}\n')
    else:
        sys.stdout.write(f'{result["result"]}\n')
sys.exit(result['code'])
