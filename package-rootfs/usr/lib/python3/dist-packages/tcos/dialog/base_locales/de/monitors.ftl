button-close = Schließen
title-main = Bildschirme
title-info = Bildschirm #{ $num }
main-info = Bildschirminformationen
monitor-number = #{ $num }
monitor-connector = Verbunden an: { $name }
monitor-resolution = Auflösung: { $resolution }