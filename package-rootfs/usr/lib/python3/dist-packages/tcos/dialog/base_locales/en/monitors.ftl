button-close = Close
title-main = Monitors
title-info = Monitor #{ $num }
main-info = Monitor information
monitor-number = #{ $num }
monitor-connector = Connected to: { $name }
monitor-resolution = Resolution: { $resolution }