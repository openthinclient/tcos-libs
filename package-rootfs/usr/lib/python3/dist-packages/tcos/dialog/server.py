#!/usr/bin/python3

import os
import sys
import uuid

import gi
import systemd.daemon
import yaml

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk, Gio, GLib, Gtk

from tcos import logger

from .config import (
    BASE_PATH, SOCKET_PATH, DIALOG_TYPES, RETURN_CODES,
    ConfigurationError, build_cfg
)
from .dialog import *


DIALOG_CLASSES = {
    'balloon':      BalloonDialog,
    'confirmation': ConfirmationDialog,
    'login':        LoginDialog,
    'monitors':     MonitorsMainDialog,
}
assert set(DIALOG_CLASSES.keys()) == set(DIALOG_TYPES)

LOG = logger.get('tcos-dialog-server')

active_dialogs = {} # {dialog: ("pid", connection, handler)}
pid_dialogs = {}    # {"pid": [dialogs]}
id_dialogs = {}     # {dialog_id: dialog}


def start_server():
    if os.path.exists(SOCKET_PATH):
        os.remove(SOCKET_PATH)
    else:
        os.makedirs(os.path.dirname(SOCKET_PATH), exist_ok=True)
    socket_server = Gio.SocketService()
    socket_server.add_address(
        Gio.UnixSocketAddress.new(SOCKET_PATH),
        Gio.SocketType.STREAM,
        Gio.SocketProtocol.DEFAULT,
        None
    )
    socket_server.connect('incoming', on_client_connect)
    LOG.debug('Serving dialogs at %s', SOCKET_PATH)
    socket_server.start()


def on_client_connect(service, connection, source_object):
    stream = connection.get_input_stream()
    reader = Gio.DataInputStream.new(stream)

    yaml_lines = []
    while True:
        line, length = reader.read_line_utf8()
        if line is None or line == '---':
            break
        yaml_lines.append(line)

    try:
        data = yaml.safe_load('\n'.join(yaml_lines))
    except Exception as ex:
        LOG.warning('Error parsing yaml: %s', ex)
        send_result(connection, RETURN_CODES.ERROR, str(ex))
        return

    if dialog_id := data.get('destroy'):
        LOG.debug('Received dialog destroy request: %s', data)
        dialog = id_dialogs.get(dialog_id)
        if dialog:
            dialog.destroy()
        send_result(connection, RETURN_CODES.OK, None)
    elif dialog_id := data.get('update'):
        LOG.debug('Received dialog update request: %s', data)
        dialog = id_dialogs.get(dialog_id)
        if not dialog:
            send_result(connection, RETURN_CODES.ERROR, 'Dialog not found')
            return
        try:
            dialog.update(data)
        except Exception as ex:
            LOG.error('Error updating dialog.', exc_info=ex)
            send_result(connection, RETURN_CODES.ERROR, str(ex))
        else:
            send_result(connection, RETURN_CODES.OK, None)
    else:
        LOG.debug('Received dialog request: %s', data)
        try:
            show_dialog(connection, data)
        except Exception as ex:
            LOG.error('Error showing dialog.', exc_info=ex)
            send_result(connection, RETURN_CODES.ERROR, str(ex))


def send_result(connection, code, result):
    yaml_data = yaml.dump({'code': code, 'result': result})
    connection.get_output_stream().write_all(yaml_data.encode())
    connection.close()


def show_dialog(connection, config):
    pid = config.get('pid')
    pid = str(pid) if pid else None
    detach = config.get('detach', False)
    try:
        dialog_config = build_cfg(config)
    except ConfigurationError as ex:
        LOG.warning('Configuration error %s', ex)
        send_result(connection, RETURN_CODES.CONFIG_ERROR, str(ex))
        return

    dialog_class = DIALOG_CLASSES[dialog_config['type']]
    if dialog_class is None:
        LOG.warning('Unknown dialog type: %s', dialog_config.get('type'))
        send_result(
                connection, RETURN_CODES.CONFIG_ERROR, 'Unknown dialog type')
        return

    try:
        dialog = dialog_class(dialog_config, on_dialog_close)
    except Exception as ex:
        LOG.error('Error creating dialog.', exc_info=ex)
        send_result(connection, RETURN_CODES.CONFIG_ERROR, str(ex))
        return

    timeout = config.get('close_timeout')
    if timeout:
        handler = GLib.timeout_add(int(timeout), on_timeout, dialog)
    else:
        handler = None

    dialog_id = uuid.uuid4().hex if detach else None
    if detach:
        id_dialogs[dialog_id] = dialog
        send_result(connection, RETURN_CODES.OK, dialog_id)
        connection = None

    active_dialogs[dialog] = (pid, connection, handler, dialog_id)
    if pid:
        pid_dialogs.setdefault(pid, []).append(dialog)

    dialog.show()


def on_dialog_close(dialog, code, result):
    # Remove dialog from active_dialogs and pid_dialogs
    pid, connection, handler, dialog_id = active_dialogs.pop(dialog, (None,)*4)

    if handler:
        GLib.source_remove(handler)

    if pid:
        pid_dialogs[pid].remove(dialog)
        if not pid_dialogs[pid]:
            pid_dialogs.pop(pid)

    if connection:
        send_result(connection, code, result)

    if dialog_id:
        id_dialogs.pop(dialog_id)


def on_timeout(dialog):
    dialog.exit(RETURN_CODES.TIMEOUT)
    return False


def check_pid_dialogs():
    try:
        if pid_dialogs:
            _check_pid_dialogs()
    except:
        LOG.exception('Error checking pid dialogs')
    return True

def _check_pid_dialogs():
    """ Find and close dialogs for no longer existent PIDs. """
    active_pids = set(os.listdir('/proc'))
    remote_pids = set(pid_dialogs.keys())

    for pid in remote_pids - active_pids:
        # Release this pid's resources
        for dialog in pid_dialogs.pop(pid):
            LOG.debug('Closing dialog for pid %s', pid)
            dialog.destroy()
            pid, connection, handler, dialog_id = active_dialogs.pop(dialog)
            if handler:
                GLib.source_remove(handler)
            if connection:
                connection.close()
            if dialog_id:
                id_dialogs.pop(dialog_id)


def init_Gtk():
    css_file = os.path.join(BASE_PATH, 'ui', 'dialog.css')
    css_provider = Gtk.CssProvider()
    css_provider.load_from_path(css_file)
    Gtk.StyleContext().add_provider_for_screen(
        Gdk.Screen.get_default(),
        css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

if __name__ == '__main__':
    if not os.path.exists('/home/tcos/.Xauthority'):
        LOG.error('Xauthority file not found')
        # Return error so that systemd will try to restart the service
        sys.exit(1)

    start_server()
    init_Gtk()
    GLib.timeout_add(300, check_pid_dialogs)
    systemd.daemon.notify('READY=1')
    GLib.MainLoop().run()
