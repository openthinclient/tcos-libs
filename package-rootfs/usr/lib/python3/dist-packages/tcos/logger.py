import logging
import os
import sys
from . import system
from .utils import program_name
import systemd.journal


def _install_exception_hook():
    prev_hook = sys.excepthook
    def hook(type, value, traceback):
        get().critical(
            'Uncaught exception',
            exc_info=(type, value, traceback)
        )
        if prev_hook is not sys.__excepthook__:
            prev_hook(type, value, traceback)
    sys.excepthook = hook


def init(name, log_to_stderr=None):
    handlers = [
        systemd.journal.JournalHandler(SYSLOG_IDENTIFIER=name)
    ]
    if log_to_stderr is None:
        # Log to stderr only if not running in systemd to avoid duplicates
        log_to_stderr = 'JOURNAL_STREAM' not in os.environ
    if log_to_stderr:
        handlers.append(logging.StreamHandler(sys.stderr))

    logging.basicConfig(
        level    = getattr(logging, system.log_level),
        handlers = handlers,
        format   ='%(message)s',
    )

    _install_exception_hook()


initialized = False
initial_name = None
def get(name=None, log_to_stderr=None):
    if name is None:
        # Create name from script path and remember it for future calls
        global initial_name
        if not initial_name:
            initial_name = program_name()
        name = initial_name

    global initialized
    if not initialized:
        init(name, log_to_stderr)
        initialized = True

    return logging.getLogger(name)


def log_initializer(method):
    # lazy initialization, so an initial get or init can occur beforehand
    def log_method(*args, **kwargs):
        log = getattr(get(), method)
        globals()[method] = log
        return log(*args, **kwargs)
    return log_method
for method in ('debug', 'info', 'warning', 'error', 'critical', 'exception'):
    globals()[method] = log_initializer(method)

from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
