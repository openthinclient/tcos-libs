import io
import os
import re
import shlex
import signal
import subprocess
import sys
from configparser import ConfigParser
from pathlib import Path

import psutil

from . import system

#
# run command and friends
#

def _map_io_arg(arg):
    if isinstance(arg, io.IOBase):
        return arg
    return arg and subprocess.PIPE or subprocess.DEVNULL


CANCEL_RESOLUTION = 0.1
def run(*command,
        stdout=False, stderr=False,
        input=None,
        log_error=True,
        expect=(None, 0,),
        check_cancel=None,
        timeout=None,
        **kwargs):
    """ Run a command and return the subprocess object.

        `stdout` and `stderr` can be either a file-like object, False to
        discard output (default) or True to capture it in the `stdout` and
        `stderr` attributes of the returned subprocess object.

        `input` is a string that is passed to the command's stdin.

        `log_error` controls logging in case of an unexpected exit code.
        It can be either a string with a custom error message, True for
        a default error message or False to disable logging. A list of
        non-error exit codes can be specified with `expect`.

        `check_cancel` is a callable to periodically test whether the command
        should be cancelled. If it returns truthy, the process is killed.

        If the command runs longer than `timeout` seconds, it is killed.

        Remaining keyword arguments are passed to subprocess.Popen.

        Returns a subprocess.CompletedProcess object. If the process was
        killed due to a timeout or cancellation, the returncode is None.
    """
    from . import logger

    if system.log_level == 'DEBUG':
        logger.get().debug('Running command: %s',
                           shlex.join(map(str, command)))

    if check_cancel and not callable(check_cancel):
        raise ValueError('check_cancel must be a callable')

    if input:
        kwargs['stdin'] = subprocess.PIPE
    kwargs.update(stdout = _map_io_arg(stdout),
                  stderr = _map_io_arg(stderr))
    kwargs.setdefault('encoding', 'utf-8')


    # Communication helper: Accumulate output
    all_stdout = []
    all_stderr = []
    def communicate(proc, input=None, timeout=None):
        out, err = proc.communicate(input, timeout)
        out and all_stdout.append(out)
        err and all_stderr.append(err),

    communicate_timeout = CANCEL_RESOLUTION if check_cancel else timeout

    # Run command to completion or until timeout
    proc = subprocess.Popen(command, **kwargs)
    while proc.poll() is None:
        try:
            communicate(proc, input, communicate_timeout)
        except subprocess.TimeoutExpired as ex:
            if (   not check_cancel  # i.e. communicate_timeout == timeout
                or check_cancel()
                or timeout and (timeout := timeout - ex.timeout) <= 0
            ):
                proc.kill()
                communicate(proc)
                proc.returncode = None
                break
        except:
            proc.kill()
            raise
        input = None  # only send input once

    # Process ended (on its own or was killed). Create CompletedProcess object
    proc = subprocess.CompletedProcess(
        command, proc.returncode,
        ''.join(all_stdout) if stdout else None,
        ''.join(all_stderr) if stderr else None
    )

    # Log error if enabled and return code is not expected
    if log_error and proc.returncode not in expect:
        if not isinstance(log_error, str):
            cmd = shlex.join(map(str, command))
            if proc.returncode is None:
                log_error = f'Command cancelled: {cmd}'
            else:
                log_error = f'Command returned error {proc.returncode}: {cmd}'
                if proc.stderr:
                    log_error = f'{log_error}\n{proc.stderr.strip()}'
        logger.error(log_error)

    return proc


#
# expand_vars
#

_VAR_PATTERN = _ESCAPED_DOLLAR = None

def expand_vars(string, on_missing=lambda var: None):
    """ Expand simple shell-like variables in a string.

        Variables are of the form $VAR and may be environment variables or
        `$HOSTNAME`. If a variable is not found, the result from `on_missing`
        (or an empty string) is used.

        Shell-like escaping is supported for `$` and `\`.
    """
    global _VAR_PATTERN, _ESCAPED_DOLLAR

    # Compile patterns only on first use
    if not _VAR_PATTERN:
        _VAR_PATTERN = re.compile(r'(?<!\\)((?:\\\\)*)\$\{?(\w+)\}?')
    if not _ESCAPED_DOLLAR:
        _ESCAPED_DOLLAR = re.compile(r'(?<!\\)((?:\\\\)*)\\\$')

    def expand(match):
        var = match.group(2)
        val = ''
        if var == 'HOSTNAME':
            val = os.uname().nodename
        elif var in os.environ:
            val = os.environ[var]
        else:
            val = str(on_missing(var) or '')

        n_o_backslashes = len(match.group(1) or '') // 2
        return '\\'*n_o_backslashes + val

    def unescape(match):
        n_o_backslashes = len(match.group(1) or '') // 2
        return '\\'*n_o_backslashes + '$'

    expanded = _VAR_PATTERN.sub(expand, string)
    return _ESCAPED_DOLLAR.sub(unescape, expanded)


def program_name():
    """ Symbolic name of the program, based on main script path. """
    path = Path(sys.argv[0])
    if path.match('/opt/*/tcos/launcher'):
        return path.parts[-3]
    elif path.match('/opt/*/tcos/*'):
        return '_'.join((path.parts[-3], path.stem))
    else:
        return path.stem


def kill_process_tree(pid: int, signal: int = 9):
    if not psutil.pid_exists(pid):
        return
    process = psutil.Process(pid)
    all_processes = (process, *process.children(recursive=True))
    for process in all_processes:
        if process.is_running():
            process.send_signal(signal)

def end_process_tree(pid: int, timeout: int = 1):
    from . import logger    # avoid circular import

    if not psutil.pid_exists(pid):
        return
    process = psutil.Process(pid)
    all_processes = (process, *process.children(recursive=True))

    logger.debug(f'Sending SIGTERM to {pid}')
    process.send_signal(signal.SIGTERM)

    if remaining := psutil.wait_procs(all_processes, timeout)[1]:
        logger.debug('Sending SIGKILL to %s',
                     ", ".join(p.name() for p in remaining))
        for process in remaining:
            try:
                process.kill()
            except psutil.NoSuchProcess:
                pass


class SingleExecutionGuard:
    def __init__(self, pid_file=None, on_active_process=end_process_tree):
        self.on_active_process = on_active_process

        if not pid_file:
            pid_file = f'{program_name()}.pid'

        if not isinstance(pid_file, Path):
            pid_file_path = Path(pid_file)

        if pid_file_path.is_absolute():
            self.pid_file_path = pid_file_path
        elif os.getuid() == 1000:
            self.pid_file_path = Path('/run/user/1000/tcos', pid_file)
        else:
            self.pid_file_path = Path('/run/tcos', pid_file)


    def __enter__(self):
        if self.pid_file_path.exists():
            pid = self.pid_file_path.read_text().strip()
            if pid and pid.isdecimal():
                pid = int(pid)
                if psutil.pid_exists(pid):
                    self.on_active_process(pid)

        self.pid_file_path.parent.mkdir(parents=True, exist_ok=True)
        self.pid_file_path.write_text(str(os.getpid()))
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        self.pid_file_path.unlink(missing_ok=True)


def setup_exit_on_signal():
    """ Exit gracefully on SIGHUP, SIGINT, SIGTERM. """
    from . import logger    # avoid circular import
    def exit_gracefully(signum, frame):
        logger.info(f"Received signal %d. Exiting.", signum)
        sys.exit(0)

    for sig in (signal.SIGHUP, signal.SIGINT, signal.SIGTERM):
        signal.signal(sig, exit_gracefully)


class IniFile(ConfigParser):
    path = None

    def __init__(self, path=None):
        super().__init__(interpolation=None)    # disable interpolation
        self.optionxform = str                  # don't mangle case
        if path:
            self.path = path
        if self.path:
            self.path = Path(self.path)
            if self.path.exists():
                self.read(self.path)

    def save(self, path=None):
        path = path or self.path
        if path:
            path = Path(path)
            path.parent.mkdir(parents=True, exist_ok=True)
            with path.open('w') as file:
                self.write(file, space_around_delimiters=False)
        else:
            from . import logger
            logger.error('No path specified to write ini file')
