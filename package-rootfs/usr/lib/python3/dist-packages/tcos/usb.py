import os
from pathlib import Path
import re
from glob import glob

from . import system


USB_CLASS_CODES = {
    '00': '(Defined at Interface level)',
    '01': 'Audio',
    '02': 'Communications',
    '03': 'Human Interface Device',
    '05': 'Physical Interface Device',
    '06': 'Imaging',
    '07': 'Printer',
    '08': 'Mass Storage',
    '09': 'Hub',
    '0a': 'CDC Data',
    '0b': 'Chip/SmartCard',
    '0d': 'Content Security',
    '0e': 'Video',
    'dc': 'Diagnostic',
    'e0': 'Wireless',
    'ef': 'Miscellaneous Device',
    'fe': 'Application Specific Interface',
    'ff': 'Vendor Specific Class'
}


USB_DEVICE_INFO_FILES = [
    'idVendor',
    'idProduct',
    'manufacturer',
    'serial',
    'product',
]


USB_INTERFACE_NAME_REGEX = re.compile(r'[0-9]+-[0-9]+(\.[0-9]+)*:[0-9]+.[0-9]+')


read = lambda p: (path := Path(p)).exists() and path.read_text().strip() or ''


def _get_usb_device_info(dev_path):
    info = {
        info_file: read(f'{dev_path}/{info_file}')
        for info_file in USB_DEVICE_INFO_FILES
    }

    device_class = read(f'{dev_path}/bDeviceClass')
    if device_class:
        if device_class == '00':
            interfaceClass = _get_usb_interface_class(dev_path)
        else:
            cls_name = USB_CLASS_CODES.get(device_class, 'unknown')
            interfaceClass = f'{device_class} {cls_name}'

        info['interfaceClass'] = interfaceClass

    return info


def _get_usb_interface_class(dev_path):
    if not os.path.exists(dev_path): return

    interfaces = [*filter(USB_INTERFACE_NAME_REGEX.match, os.listdir(dev_path))]
    if len(interfaces) == 0: return ''

    cls = read(f'{dev_path}/{interfaces[0]}/bInterfaceClass')
    cls_name = USB_CLASS_CODES.get(cls, "unknown")

    return f'{cls} {cls_name}'


def _get_usb_subdevices(dev_path):
    busnum = read(f'{dev_path}/busnum')
    return  (
        file for file in os.listdir(dev_path)
        if re.match(rf'{busnum}-[0-9]+(\.[0-9]+)*$', file)
    )

def devices_by_vendor(idVendor):
    return [ dev for dev in devices() if dev.get('idVendor', '') == idVendor ]


from time import monotonic
DEVICE_CACHE_TIME = .3 # seconds
cached_devices = None
cached_devices_timeout = 0
def devices():
    global cached_devices, cached_devices_timeout

    if cached_devices is None or cached_devices_timeout <= monotonic():
        cached_devices = (*_devices(),)
        cached_devices_timeout = monotonic() + DEVICE_CACHE_TIME

    return cached_devices


def _devices(dev_paths=None):
    if dev_paths is None:
        dev_paths = glob('/sys/bus/usb/devices/usb*')

    for dev in dev_paths:
        yield _get_usb_device_info(dev)

        try:
            sub_devs = _get_usb_subdevices(dev)
        except OSError:
            sub_devs = ()

        sub_dev_paths = (f'{dev}/{sub}' for sub in sub_devs)
        yield from _devices(sub_dev_paths)


def get_smartcard_readers():
    return [ dev for dev in devices()
             if ( '0c4b' in dev['idVendor'] # Reiner SCT
                  or 'Chip/SmartCard' in dev.get('interfaceClass', '') ) ]


GRUNDIG_DEVICE_IDS = ('15d8',)
def get_grundig_devices():
    return devices_by_vendor('15d8')


OLYMPUS_DEVICE_IDS = ('07b4',)
def get_olympus_devices():
    return devices_by_vendor('07b4')


PHILIPS_DEVICE_IDS = ('0911',)
def get_philips_devices():
    return devices_by_vendor('0911')


SIGNOTEC_DEVICE_IDS = ('2133',)
def get_signotec_devices():
    return devices_by_vendor('2133')


# StepOver device IDs as per
# https://www.stepoverinfo.net/confluence/pages/viewpage.action?pageId=58654782
STEPOVER_DEVICE_IDS = (
    '22c9:fe12', '22c9:0001', '22c9:0002',                          # Bootloader
    '1690:2000', '1690:2001',                                       # G5
    '22c9:0601', '2689:0601', '22c9:06e1', '2689:06e1', '268e:0601',# G6
    '22c9:0701', '2689:0701', '22c9:07e1', '2689:07e1',             # G7 / G10
    '22c9:0801', '2689:0801', '22c9:0881', '2689:0881',             # G8
    '22c9:0901', '2689:0901', '22c9:09e1', '2689:09e1',             # G9
    '22c9:0ce1', '22c9:0cf1', '2689:0cf1', '2689:0ce1',             # G12
    '22c9:0d01', '2689:0d01', '22c9:0df1', '2689:0df1',             # G13
    '22C9:1001', '2689:1001',                                       # NextGen
)
def get_stepover_devices():
    return [dev for dev in devices()
            if f'{dev["idVendor"]}:{dev["idProduct"]}' in STEPOVER_DEVICE_IDS]
