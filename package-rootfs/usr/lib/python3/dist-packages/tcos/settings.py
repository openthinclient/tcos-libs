import json
import socket
import time

from . import logger

SOCKET_PATH='/run/tcos/settings.sock'
MAX_RETRIES=30 # deciseconds


def read_tcosd(cmd, raw=False):
    for attempt in range(MAX_RETRIES):
        try:
            data = _read_tcosd(cmd)
            return data if raw else json.loads(data)
        except OSError as ex:
            if attempt == MAX_RETRIES - 1:
                raise ex
            if attempt % 10 == 0:
                logger.get().warning(
                        f'Failed to connect to tcosd: {ex.strerror}.'
                        f' Retrying... (Attempt {attempt+1}/{MAX_RETRIES})')
            time.sleep(.1)
        except Exception as ex:
            logger.get().exception(
                    f'Failed to get {cmd} from tcosd: {ex}')
            raise ex


def _read_tcosd(cmd):
    chunks = []
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        sock.connect(SOCKET_PATH)
        sock.sendall(cmd.encode('utf-8'))
        sock.send(b'\n')

        while True:
            chunk = sock.recv(4096)
            if not chunk:
                return b''.join(chunks)
            chunks.append(chunk)


def store(dir, mac=None):
    if mac:
        cmd = f'store {dir} {mac}'
    else:
        cmd = f'store {dir}'
    result = read_tcosd(cmd, raw=True)
    return True if result == b'0' else False


app_defaults = {}
def get_app_defaults(app_type):
    if not app_type in app_defaults:
        app_defaults[app_type] = read_tcosd(f'defaults application/{app_type}')
    return app_defaults[app_type] or {}


apps = None
def get_apps(name=None, type=None):
    global apps
    if apps is None:
        apps = read_tcosd('apps')

    result = apps
    if type is not None:
        type = f'application/{type}'
        result = [app for app in result if app['type'] == type]
    if name is not None:
        result = [app for app in result if app['name'] == name]
    return result

def get_app(name=None, type=None):
    """ Return first app with name and/or type or None if no app found. """
    apps = get_apps(name, type)
    return apps[0] if apps else None


def get_apps_for_mac(mac, name=None, type=None):
    result = read_tcosd(f'apps-for-mac {mac}')

    if type is not None:
        type = f'application/{type}'
        result = [app for app in result if app['type'] == type]
    if name is not None:
        result = [app for app in result if app['name'] == name]
    return result

def get_app_for_mac(mac, name=None, type=None):
    apps = get_apps_for_mac(mac, name, type)
    return apps[0] if apps else None


printers = None
def get_printers(type=None):
    global printers
    if printers is None:
        printers = read_tcosd('printers')
    if type is None:
        return printers
    type = f'printer/{type}'
    return [printer for printer in printers if printer['type'] == type]


devices = None
def get_devices(type=None):
    global devices
    if devices is None:
        devices = read_tcosd('devices')
    if type is None:
        return devices
    type = f'device/{type}'
    return [device for device in devices if device['type'] == type]


client_settings = None
def get_client(key=None):
    global client_settings
    if client_settings is None:
        client_settings = read_tcosd('client')
    if key is None:
        return client_settings
    return client_settings[key]

login_type = None
def get_login_type():
    global login_type
    if login_type is None:
        login_type = read_tcosd('login-type', raw=True).decode('utf-8')
    return login_type

def set_login_type(login_type):
    read_tcosd(f'login-type {login_type}', raw=True)

def get_sso():
    """ Return a tuple with username and password of the current user. """
    return read_tcosd('sso', raw=True).decode('utf-8').split('\n')


def get_client_name():
    try:
        return get_client('name')
    except Exception as ex:
        logger.get().exception(f'Failed to get client name: {ex}')

    return 'openthinclient'
