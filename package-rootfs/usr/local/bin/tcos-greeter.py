#!/usr/bin/env python3
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('LightDM', '1')

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import LightDM
from time import strftime
import os
from tcos.utils import run


UI_FILE_LOCATION = '/usr/local/share/tcos-greeter/tcos-greeter.ui'
CSS_FILE_LOCATION = '/usr/local/share/tcos-greeter/tcos-greeter.css'

translation = {
    'label_user': ['User:', 'Benutzer:'],
    'password_label': ['Password:', 'Passwort:'],
    'login_button': ['Login', 'Einloggen'],
    'reboot_button': ['Reboot', 'Neustarten'],
    'poweroff_button': ['Power Off', 'Ausschalten'],
}


clock = None

user_entry = None
greeter = None
login_clicked = False
password_entry = None
password_label = None
errorLabel = None

login_no = 0

builder = None

current_lang = 'en'


def update_clock():
    if clock == None: return False

    clock.set_text(strftime('%a, %d %b %H:%M:%S'))

    return True


def set_language(lang='en'):
    index = 1 if lang == 'de' else 0

    global current_lang
    current_lang = lang

    label_user_text      = get_translation_text('label_user', index)
    password_label_text  = get_translation_text('password_label', index)
    login_button_text    = get_translation_text('login_button', index)
    reboot_button_text   = get_translation_text('reboot_button', index)
    poweroff_button_text = get_translation_text('poweroff_button', index)

    builder.get_object('label_user').set_text(label_user_text)
    builder.get_object('password_label').set_text(password_label_text)
    builder.get_object('login_button').set_label(login_button_text)
    builder.get_object('reboot_button').set_label(reboot_button_text)
    builder.get_object('poweroff_button').set_label(poweroff_button_text)


def get_translation_text(key, index):
    if translation.get(key, None) is not None:
        return translation[key][index]
    else:
        try:
            return str(key)
        except:
            return 'translation not found'


def poweroff_click_handler(widget, data=None):
    if LightDM.get_can_shutdown():
        LightDM.shutdown()


def reboot_click_handler(widget, data=None):
    if LightDM.get_can_restart():
        LightDM.restart()


def login_click_handler(widget, data=None):
    global login_clicked
    login_clicked = True

    if greeter.get_is_authenticated():
        start_session()

    if greeter.get_in_authentication():
        greeter.cancel_authentication()

    username = user_entry.get_text()

    errorLabel.set_text('')
    greeter.authenticate(username)


def authentication_complete(greeter):
    if login_clicked:
        if greeter.get_is_authenticated():
            start_session()
        else:
            display_error_message('Login failed')


def show_prompt(greeter, text, prompt_type=None, **kwargs):
    if login_clicked:
        greeter.respond(password_entry.get_text())
        password_entry.set_text('')


def start_session():
    greeter.start_session_sync('')


def show_message(greeter, text, message_type=None, **kwargs):
    errorLabel.set_text(text)


def display_error_message(text, duration=5000):
    if current_lang == 'de':
        errorLabel.set_text('Login fehlgeschlagen')
    else:
        errorLabel.set_text('Login failed')

    global login_no
    login_no += 1
    local_login_no = login_no

    def inner():
        global login_no
        if local_login_no != login_no:
            return False

        errorLabel.set_text('')
        return False

    GLib.timeout_add(duration, inner)


if __name__ == '__main__':
    settings = Gtk.Settings.get_default()
    settings.set_property('gtk-icon-theme-name', 'ContrastHigh')

    builder = Gtk.Builder()
    greeter = LightDM.Greeter()
    greeter.connect('authentication-complete', authentication_complete)
    greeter.connect('show-prompt', show_prompt)
    greeter.connect('show-message', show_message)

    builder.add_from_file(UI_FILE_LOCATION)

    cssProvider = Gtk.CssProvider()
    cssProvider.load_from_path(CSS_FILE_LOCATION)
    styleContext = Gtk.StyleContext()
    screen = Gdk.Screen.get_default()

    styleContext.add_provider_for_screen(
        screen,
        cssProvider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    poweroffButton = builder.get_object('poweroff_button')
    poweroffButton.connect('activate', poweroff_click_handler)

    rebootButton = builder.get_object('reboot_button')
    rebootButton.connect('activate', reboot_click_handler)

    password_entry = builder.get_object('password_entry')
    password_entry.connect('activate', login_click_handler)
    password_label = builder.get_object('password_label')

    errorLabel = builder.get_object('error_label')

    loginButton = builder.get_object('login_button')
    loginButton.connect('clicked', login_click_handler)

    user_entry = builder.get_object('user_entry')

    if os.environ.get('LANG') == 'de_DE.UTF-8':
        set_language('de')
    else:
        set_language('en')

    ipLabel = builder.get_object('ip_label')
    ipLabel.set_text(run('/usr/local/bin/tcos-ip', stdout=True).stdout)

    window = builder.get_object('window')

    greeter.connect_to_daemon_sync()

    clock = builder.get_object('clock')
    GLib.timeout_add(100, update_clock)

    screen.get_root_window().set_cursor(Gdk.Cursor(Gdk.CursorType.LEFT_PTR))

    primary_monitor = Gdk.Display.get_default().get_primary_monitor().get_workarea()
    window.move(primary_monitor.x, primary_monitor.y)
    window.resize(primary_monitor.width, primary_monitor.height)

    user_entry.grab_focus()

    window.show()

    GLib.MainLoop().run()
