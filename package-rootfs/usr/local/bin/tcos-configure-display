#!/usr/local/bin/tcos-python

from tcos import system, settings, logger
from tcos.dialog import deferred as dialog
from tcos.utils import run

import re
import sys
import subprocess
import time


# Log only to journal. This script is called by lightdm which will repeat
# stderr output to the journal.
LOG = logger.get(log_to_stderr=False)


def _defer_config_error_dialog(message_key, **values):
    dialog.defer(
        dir='/opt/tcos-libs/tcos/dialog',
        type='config-error',
        message_key=message_key,
        values=values
    )


match_monitor_line = re.compile(r'(\S+)\s+((?:dis)?connected)\b').match
match_mode_line    = re.compile(r'\s{3}(\S+)\s+[\d\.]+(\*?)(\+?)').match

ends_with_digits      = re.compile(r'(\d+)$')
ends_with_dash_digits = re.compile(r'-(\d+)$')


def get_randr_info():
    proc = run(
        '/usr/bin/xrandr', '-q', '-d', ':0',
        stdout=True, stderr=True
    )

    if proc.returncode != 0:
        LOG.error('Failed to get xrandr info: %s', proc.stderr)
        sys.exit(1)

    result = {}
    cur_connector = None

    for line in proc.stdout.splitlines():
        match = match_monitor_line(line)
        if match:
            cur_connector = match.group(1)
            result[cur_connector] = {
                'status': match.group(2),
                'resolutions': [],
            }
            continue

        if cur_connector is None:
            continue

        match = match_mode_line(line)
        if match:
            result[cur_connector]['resolutions'].append(match.group(1))
            if match.group(3):
                result[cur_connector]['preferred'] = match.group(1)

    return result


def run_custom_xrandr_command(xrandr_cmd):
    LOG.debug('Executing xrandr command: %s', xrandr_cmd)
    try:
        subprocess.check_call(xrandr_cmd, shell=True)
        return True
    except subprocess.CalledProcessError as ex:
        LOG.error('Custom xrandr command failed: %s', ex)
        return False


def reset_xrandr_and_exit():
    """ Last ditch effort in case of errors so the end user does not end
        up with black screens.
    """
    proc = run('/usr/bin/xrandr', '-d', ':0', '--auto', stderr=True)
    if proc.returncode != 0:
        LOG.error('Failed to execute xrandr --auto: %s', proc.stderr)
        sys.exit(0)


def get_connector(connector, randr_info):
    if connector in ('', 'disabled', 'NONE'):
        return 'disabled'

    if connector in randr_info:
        return connector

    if ends_with_dash_digits.search(connector):
        connector = ends_with_dash_digits.sub(r'\1', connector)
    elif ends_with_digits.search(connector):
        connector = ends_with_digits.sub(r'-\1', connector)

    if connector in randr_info:
        return connector

    return None


def switch_all_off_cmd(randr_info):
    connectors = list(connected(randr_info))
    if not connectors:
        return

    cmd = ['/usr/bin/xrandr', '-d', ':0']
    for connector in connectors:
        cmd.extend(['--output', connector, '--off'])

    return cmd


def connected(randr_info):
    for connector, info in randr_info.items():
        if info['status'] == 'connected':
            yield connector


parse_modeline = re.compile(r'(?:#.*\n)*\s*Modeline\s+\S+\s+(.*)').match

def get_mode(conf_key, settings, connector, randr_info):
    """ Get the mode to use for the given connector.

        If the mode is not available, it will be created and added to the
        connector.
    """
    available_modes = randr_info[connector]['resolutions']
    freq = settings.get(f'{conf_key}.fallbackfrequency', '').strip()
    mode = settings.get(f'{conf_key}.resolution', '').strip()
    width, height = mode.split('x')

    if not mode:
        mode = randr_info[connector].get('preferred', available_modes[0])

    if mode in available_modes:
        return mode

    if mode and freq:
        # Adjust mode name and try again (in case we already created the mode)
        mode = f'{mode}_{freq}'
        if mode in available_modes:
            return mode

    # Create mode
    if freq:
        proc = run('/usr/bin/cvt', '-r', width, height, freq,
                          stdout=True)
    else:
        proc = run('/usr/bin/cvt', width, height, stdout=True)

    if proc.returncode != 0:
        LOG.error('Failed to generate mode for %s: %s', mode, proc.stderr)
        sys.exit(0)

    modeline = parse_modeline(proc.stdout).group(1)

    proc = run(
        '/usr/bin/xrandr', '--newmode', mode, *modeline.split(),
        stdout=True, stderr=True
    )

    if proc.returncode != 0:
        LOG.error('Failed to add mode %s: %s', mode, proc.stderr)
        sys.exit(0)

    proc = run(
        '/usr/bin/xrandr', '--addmode', connector, mode,
        stdout=True, stderr=True
    )

    if proc.returncode != 0:
        LOG.error('Failed to add mode %s to connector %s: %s',
                  mode, connector, proc.stderr)
        sys.exit(0)

    return mode


def configure_display_cmd(conf_key, display_settings, randr_info):
    connector_conf = (
            display_settings.get(f'{conf_key}.cust_connector', '').strip()
        or  display_settings.get(f'{conf_key}.connect', '').strip()
    )
    connector = get_connector(connector_conf, randr_info)
    if not connector:
        LOG.error(f'Failed to find connector for {conf_key} in {randr_info}')
        _defer_config_error_dialog(
            'display-connector-not-found',
            connector=connector_conf,
            connectors='\n\n\n'.join(randr_info.keys())
        )
        sys.exit(0)

    if connector == 'disabled':
        return

    mode = get_mode(conf_key, display_settings, connector, randr_info)

    cmd = [
        '/usr/bin/xrandr', '-d', ':0',
        '--output', connector,
        '--mode', mode,
        '--rotate', display_settings.get(f'{conf_key}.rotation', 'normal'),
    ]
    if conf_key == 'firstscreen':
        cmd.append('--primary')
    elif conf_key == 'secondscreen':
        pos = display_settings.get(f'{conf_key}.positioning', '--right-of')
        other_con_conf = (
                display_settings.get('firstscreen.cust_connector', '').strip()
            or  display_settings.get('firstscreen.connect', '').strip()
        )
        other_con = get_connector(other_con_conf, randr_info)
        cmd.append(pos)
        cmd.append(other_con)

        if pos == '--same-as':
            # Scale 2nd screen to match 1st screen if display is cloned
            first_res = display_settings.get('firstscreen.resolution')
            secnd_res = display_settings.get('secondscreen.resolution')
            if first_res != secnd_res:
                try:
                    first_x, first_y = map(int, first_res.split('x'))
                    secnd_x, secnd_y = map(int, secnd_res.split('x'))
                    scale_x = first_x / secnd_x
                    scale_y = first_y / secnd_y
                    cmd.append('--scale')
                    cmd.append(f'{scale_x}x{scale_y}')
                except Exception as ex:
                    LOG.error('Failed to compute scaling 2nd screen', ex)

    return cmd


def main():
    display_settings = settings.get_devices('display')
    if not display_settings:
        LOG.debug('No display settings found. Exiting.')
        sys.exit(0)
    display_settings = display_settings[0]

    xrandr_cmd = display_settings.get('xrandrcmd', '').strip()
    if xrandr_cmd:
        run_custom_xrandr_command(xrandr_cmd) or reset_xrandr_and_exit()
        sys.exit(0)

    randr_info = get_randr_info()

    commands = (
        switch_all_off_cmd(randr_info),
        configure_display_cmd('firstscreen', display_settings, randr_info),
        configure_display_cmd('secondscreen', display_settings, randr_info),
    )

    for cmd in filter(None, commands):
        if not cmd:
            continue
        LOG.debug('Executing xrandr command: %s', cmd)
        proc = run(*cmd, stderr=True)
        time.sleep(.1) # Magic: This eliviates driver problems on some systems
        if proc.returncode != 0:
            LOG.error('Failed to execute command: %s', proc.stderr)
            reset_xrandr_and_exit()


if __name__ == '__main__':
    # Catch any errors so we do not ever return a non-zero exit code, which
    # would result in (possibly infinite) restarts of the lightdm service.
    try:
        main()
    except Exception as ex:
        LOG.error('Failed while configuring display: %s', ex)
        reset_xrandr_and_exit()
