#!/usr/local/bin/tcos-python

import base64
import os
import socketserver
import ldap3
from tcos import settings, system, logger

SOCKET_PATH = '/run/tcos/auth.sock'

LOG = logger.get()


class LDAPServer:
    def __init__(self, url, binddn=None, bindpw=None):
        ldap_info = self.parse_ldapurl(url)
        self.base = ldap_info['base']
        self.binddn = binddn or ldap_info['binddn']
        self.bindpw = bindpw or ldap_info['bindpw']
        self.server = ldap3.Server(
            ldap_info['host'],
            port=ldap_info['port'],
            use_ssl=ldap_info['ssl'],
        )

    def connection(self, binddn = None, bindpw = None):
        return ldap3.Connection(
            self.server,
            binddn or self.binddn,
            bindpw or self.bindpw,
            auto_bind=True,
            check_names=False,
            read_only=True,
        )

    @staticmethod
    def parse_ldapurl(url):
        # Parse extensions manually, due to ldap3's parse_uri getting confused
        # by commas (in bindname).
        if '????' not in url:
            return ldap3.utils.uri.parse_uri(url)

        uri, exts = url.split('????', 1)
        ldap_info = ldap3.utils.uri.parse_uri(uri)
        for ext in exts.split(','):
            key, val = ldap3.utils.uri.unquote(ext).split('=', 1)
            if key == 'X-BINDPW':
                ldap_info['bindpw'] = base64.b64decode(val)
            elif key == 'bindname':
                ldap_info['binddn'] = val
            else:
                LOG.warn(f'Unknown extension "{key}" in ldapurl boot argument')
        return ldap_info


def check_auth(ldap_server, name, password, user_filter, user_attr):
    esc_name = ldap3.utils.conv.escape_filter_chars(name, 'utf-8')

    with ldap_server.connection() as conn:
        success = conn.search(
            ldap_server.base,
            f'(&{user_filter}({user_attr}={esc_name}))',
            ldap3.SUBTREE,
            size_limit=1,
        )
        if not success:
            LOG.info(f'User {name} not found')
            return b''
        try:
            dn = conn.entries[0].entry_dn
            with ldap_server.connection(dn, password):
                LOG.info(f'User {dn} authenticated')
                return dn.encode('utf-8')
        except ldap3.core.exceptions.LDAPBindError as ex:
            LOG.info(f'User {name} not authenticated: %s', ex)
            return b''


class SocketHandler(socketserver.StreamRequestHandler):
    def handle(self):
        name     = self.rfile.readline().rstrip(b'\n').decode('utf-8')
        password = self.rfile.readline().rstrip(b'\n').decode('utf-8')
        client = settings.get_client()
        try:
            ldap = None
            if client['UserGroupSettings.DirectoryVersion'] == 'secondary':
                conf = 'Directory.Secondary'
                url    = client.get(f'{conf}.LDAPURLs', '').strip()
                binddn = client.get(f'{conf}.ReadOnly.Principal', '').strip()
                bindpw = client.get(f'{conf}.ReadOnly.Secret', '')
                user_attr   = client.get(f'{conf}.UserNameAttribute', '').strip()
                user_filter = client.get(f'{conf}.UserObjectFilter', '').strip()
                if not user_filter.startswith('('):
                    user_filter = f'(objectClass={user_filter})'
                if url and binddn and bindpw and user_attr and user_filter:
                    ldap = LDAPServer(url, binddn, bindpw)
                    if ldap:
                        LOG.debug('Authenticating "%s" against %s', name, url)
                    else:
                        LOG.error('Failed to connect to %s', url)

            if not ldap:
                LOG.debug('Authenticating "%s" against built-in LDAP', name)
                ldap = LDAPServer(system.boot_arguments['ldapurl'])
                user_filter='(objectClass=person)'
                user_attr='cn'

            dn = check_auth(ldap, name, password, user_filter, user_attr)
            self.wfile.write(dn)
            self.wfile.close()

        except Exception as e:
            import traceback
            traceback.print_exc()
            LOG.error(f'Error authenticating user: {e}')
            self.wfile.close()


class SocketServer(socketserver.UnixStreamServer):
    def __init__(self):
        if os.path.exists(SOCKET_PATH):
            os.remove(SOCKET_PATH)
        else:
            os.makedirs(os.path.dirname(SOCKET_PATH), exist_ok=True)

        super().__init__(SOCKET_PATH, SocketHandler)

        os.chmod(SOCKET_PATH, 0o666)


if __name__ == '__main__':
    SocketServer().serve_forever()
