#!/usr/bin/python3
import os
from tcos import logger
from tcos.utils import run

CUSTOM_CERT_DIR = '/tcos/link/custom/system-certs'
LOCAL_CERT_DIR = '/usr/local/share/ca-certificates'

LOG = logger.get()

if not os.path.isdir(CUSTOM_CERT_DIR):
    exit()

found_certificates = False
os.chdir(CUSTOM_CERT_DIR)
for cert_file in os.listdir(CUSTOM_CERT_DIR):
    target_file_path = os.path.join(LOCAL_CERT_DIR, cert_file)
    cert_file_path = os.path.join(CUSTOM_CERT_DIR, cert_file)

    # `update-ca-certificates` insists on .crt extension
    if not cert_file.endswith('.crt'):
        target_file_path += '.crt'

    # Ensure unique file name to avoid overwriting existing certificates
    uniquifier = 1
    while os.path.exists(target_file_path):
        target_file_path = f'{target_file_path[:-4]}-{uniquifier}.crt'
        uniquifier += 1

    # Try to convert the certificate to PEM format
    proc = run(
        '/usr/bin/openssl', 'x509',
        '-in',  cert_file_path,   '-inform',  'DER',
        '-out', target_file_path, '-outform', 'PEM',
        log_error = False
    )
    if proc.returncode == 0:
        found_certificates = True
        continue

    # Test if it's already in PEM format
    proc = run(
        '/usr/bin/openssl', 'x509', '-in', cert_file_path,
        log_error = False
    )
    if proc.returncode == 0:
        found_certificates = True
        os.symlink(cert_file_path, target_file_path)
        continue

    LOG.warning('Ignoring unrecognized certificate: %s', cert_file_path)

if found_certificates:
    run('/usr/sbin/update-ca-certificates', stdout=None, stderr=None)
