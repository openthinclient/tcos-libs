window-title = Systeminformationen

usb-devices-tab-title = USB-Geräte
general-info-tab-title = Allgemeines
net-devices-tab-title = Netzwerk

no-usb-devices = Keine USB-Geräte gefunden.

hostname = Host-Name
mac-address = Client-ID / MAC-Adresse
management-server = Management-Server
boot-method = Boot-Methode
is-localboot = Localboot
is-localboot-installer = Localboot-Installer
pxe-boot = PXE
os-version = Betriebssystemversion
open-management = Management öffnen

ipv4 = IPv4-Adresse
ipv6 = IPv6-Adresse
rx-errors = RX Fehlerhafte-Pakete
rx-dropped = RX Verworfene-Pakete
tx-errors = TX Fehlerhafte-Pakete
tx-dropped = TX Verworfene-Pakete
not-connected = nicht verbunden
not-wired = kein Kabel
