window-title = System information

usb-devices-tab-title = USB Devices
general-info-tab-title = General
net-devices-tab-title = Network

no-usb-devices = No USB devices found.

hostname = Hostname
mac-address = Client ID / MAC address
management-server = Management server
boot-method = Boot method
is-localboot = Localboot
is-localboot-installer = Localboot installer
pxe-boot = PXE
os-version = Operating system version
open-management = Open management

ipv4 = IPv4 address
ipv6 = IPv6 address
rx-errors = RX errors
rx-dropped = RX dropped
tx-errors = TX errors
tx-dropped = TX dropped
not-connected = no connection
not-wired = cable disconnected
